# Assets

Mostly RMC-related.
Both actual and obsolete.

EVE Online assets can be downloaded [here](https://www.dropbox.com/sh/lcyblx0pe3vhq4v/AABnyxAFIW5C2ctKT4yMvM03a?dl=0)

## License

Note: This content is not licensed. Permission might be required.

EVE Online and its content © CCP ehf.
